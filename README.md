# Nepenthes

Nepenthes is an open-source project management suite.

## License

Nepenthes is licensed under the terms of the GNU General Public License version 3.
See [COPYRIGHT](COPYRIGHT) and [LICENSE](LICENSE) files for details.

## Credits

### Icons

Thanks to Vincent Le Moign and his fabulous Minicons icons on [webalys.com](http://www.webalys.com/minicons/icons-free-pack.php).

### OpenProject icon font

Published and created by the OpenProject Foundation (OPF) under [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/)
with icons from the following sources
[Minicons Free Vector Icons Pack](http://www.webalys.com/minicons) and
[User Interface Design framework](http://www.webalys.com/design-interface-application-framework.php) both by webalys

**Creative Commons License**

OpenProject Icon Font by the OpenProject Foundation (OPF) is licensed under Creative Commons Attribution 3.0 Unported License
and Free for both personal and commercial use. You can copy, adapt, remix, distribute or transmit it.

Under this condition: provide a mention of the "OpenProject Foundation" and a link back to OpenProject www.openproject.org.
