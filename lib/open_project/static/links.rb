#-- copyright
# Nepenthes is a free and open project management software.
# Copyright (C) 2024 The Nirvati developers
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License version 3.
#
# Nepenthes is a fork of OpenProject. The copyright follows:
#
# Copyright (C) 2012-2024 the OpenProject GmbH
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License version 3.
#
# OpenProject is a fork of ChiliProject, which is a fork of Redmine. The copyright follows:
# Copyright (C) 2006-2013 Jean-Philippe Lang
# Copyright (C) 2010-2013 the ChiliProject Team
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# See COPYRIGHT and LICENSE files for more details.
#++

module OpenProject
  module Static
    module Links
      class << self
        def help_link_overridden?
          OpenProject::Configuration.force_help_link.present?
        end

        def help_link
          OpenProject::Configuration.force_help_link.presence || static_links[:user_guides]
        end

        delegate :[], to: :links

        def links
          @links ||= static_links.merge(dynamic_links)
        end

        def url_for(item)
          links.dig(item, :href)
        end

        def has?(name)
          @links.key? name
        end

        private

        def dynamic_links
          dynamic = {
            help: {
              href: help_link,
              label: "top_menu.help_and_support"
            }
          }

          if impressum_link = OpenProject::Configuration.impressum_link
            dynamic[:impressum] = {
              href: impressum_link,
              label: "homescreen.links.impressum"
            }
          end

          dynamic
        end

        def static_links
          {
            user_guides: {
              href: "https://nepenthes.nirvati.org/user-guide/",
              label: "homescreen.links.user_guides"
            },
            installation_guides: {
              href: "https://nepenthes.nirvati.org/installation-and-operations/installation/",
              label: :label_installation_guides
            },
            packager_installation: {
              href: "https://nepenthes.nirvati.org/installation-and-operations/installation/packaged/",
              label: "Packaged installation"
            },
            docker_installation: {
              href: "https://nepenthes.nirvati.org/installation-and-operations/installation/docker/",
              label: "Docker installation"
            },
            manual_installation: {
              href: "https://nepenthes.nirvati.org/installation-and-operations/installation/manual/",
              label: "Manual installation"
            },
            upgrade_guides: {
              href: "https://nepenthes.nirvati.org/installation-and-operations/operation/upgrading/",
              label: :label_upgrade_guides
            },
            postgres_migration: {
              href: "https://nepenthes.nirvati.org/installation-and-operations/misc/packaged-postgresql-migration/",
              label: :"homescreen.links.postgres_migration"
            },
            postgres_13_upgrade: {
              href: "https://nepenthes.nirvati.org/installation-and-operations/misc/migration-to-postgresql13/"
            },
            configuration_guide: {
              href: "https://nepenthes.nirvati.org/installation-and-operations/configuration/",
              label: "links.configuration_guide"
            },
            contact: {
              href: "https://www.openproject.org/contact/",
              label: "links.get_in_touch"
            },
            glossary: {
              href: "https://nepenthes.nirvati.org/glossary/",
              label: "homescreen.links.glossary"
            },
            shortcuts: {
              href: "https://nepenthes.nirvati.org/user-guide/keyboard-shortcuts-access-keys/",
              label: "homescreen.links.shortcuts"
            },
            website: {
              href: "https://nepenthes.nirvati.org/",
              label: "label_openproject_website"
            },
            blog: {
              href: "https://nepenthes.nirvati.org/blog/",
              label: "homescreen.links.blog"
            },
            blog_article_progress_changes: {
              href: "https://www.openproject.org/blog/changes-progress-work-estimates/",
              label: "Significant changes to progress and work estimates"
            },
            release_notes: {
              href: "https://nepenthes.nirvati.org/release-notes/",
              label: :label_release_notes
            },
            report_bug: {
              href: "https://nepenthes.nirvati.org/development/report-a-bug/",
              label: :label_report_bug
            },
            weblate: {
              href: "https://nepenthes.nirvati.org/development/translate-openproject/",
              label: :label_add_edit_translations
            },
            api_docs: {
              href: "https://nepenthes.nirvati.org/api/",
              label: :label_api_doc
            },
            text_formatting: {
              href: "https://nepenthes.nirvati.org/user-guide/wysiwyg/",
              label: :setting_text_formatting
            },
            oauth_authorization_code_flow: {
              href: "https://oauth.net/2/grant-types/authorization-code/",
              label: "oauth.flows.authorization_code"
            },
            client_credentials_code_flow: {
              href: "https://oauth.net/2/grant-types/client-credentials/",
              label: "oauth.flows.client_credentials"
            },
            ldap_encryption_documentation: {
              href: "https://www.rubydoc.info/gems/net-ldap/Net/LDAP#constructor_details"
            },
            origin_mdn_documentation: {
              href: "https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Origin"
            },
            security_badge_documentation: {
              href: "https://nepenthes.nirvati.org/system-admin-guide/information/#security-badge"
            },
            date_format_settings_documentation: {
              href: "https://nepenthes.nirvati.org/system-admin-guide/calendars-and-dates/#date-format"
            },
            webinar_videos: {
              href: "https://www.youtube.com/watch?v=un6zCm8_FT4"
            },
            get_started_videos: {
              href: "https://www.youtube.com/playlist?list=PLGzJ4gG7hPb8WWOWmeXqlfMfhdXReu-RJ"
            },
            openproject_docs: {
              href: "https://nepenthes.nirvati.org/"
            },
            progress_tracking_docs: {
              href: "https://nepenthes.nirvati.org/user-guide/time-and-costs/progress-tracking/"
            },
            storage_docs: {
              setup: {
                href: "https://nepenthes.nirvati.org/system-admin-guide/integrations/storage/"
              },
              nextcloud_setup: {
                href: "https://nepenthes.nirvati.org/system-admin-guide/integrations/nextcloud/"
              },
              one_drive_setup: {
                href: "https://nepenthes.nirvati.org/system-admin-guide/integrations/one-drive/"
              },
              one_drive_drive_id_guide: {
                href: "https://nepenthes.nirvati.org/system-admin-guide/integrations/one-drive/drive-guide/"
              },
              nextcloud_oauth_application: {
                href: "https://apps.nextcloud.com/apps/integration_nepenthes"
              },
              one_drive_oauth_application: {
                href: "https://portal.azure.com/"
              },
              troubleshooting: {
                href: "https://www.openproject.org/docs/user-guide/file-management/nextcloud-integration/#possible-errors-and-troubleshooting"
              }
            },
            ical_docs: {
              href: "https://nepenthes.nirvati.org/user-guide/calendar/#subscribe-to-a-calendar"
            },
            integrations: {
              href: "https://nepenthes.nirvati.org/system-admin-guide/integrations/"
            }
          }
        end
      end
    end
  end
end
