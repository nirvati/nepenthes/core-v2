const PROXY_HOSTNAME = process.env.PROXY_HOSTNAME || '127.0.0.1';

const PROXY_CONFIG = [
  {
    "context": (path) => /^(?!\/assets\/frontend).*\/.*$/.test(path),
    "target": `http://${PROXY_HOSTNAME}:3000`,
    "secure": false,
    "timeout": 360000,
    // "bypass": function (req, res, proxyOptions) {
    // }
  }
];

module.exports = PROXY_CONFIG;
