import { ErrorReporterBase } from 'core-app/core/errors/error-reporter-base';
import { LocalReporter } from 'core-app/core/errors/local/local-reporter';

export function configureErrorReporter():ErrorReporterBase {
  return new LocalReporter();
}
