export const contactUrl:{ [locale:string]:string } = {
  en: 'https://www.openproject.org/contact/',
  de: 'https://www.openproject.org/de/kontakt/',
};

