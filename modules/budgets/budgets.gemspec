Gem::Specification.new do |s|
  s.name        = "budgets"
  s.version     = "1.0.0"
  s.authors     = ["The Nepenthes developers"]
  s.summary     = "Nepenthes Budgets."

  s.files = Dir["{app,config,db,lib}/**/*"]
  s.metadata["rubygems_mfa_required"] = "true"
end
