Gem::Specification.new do |s|
  s.name        = "grids"
  s.version     = "1.0.0"
  s.authors     = ["Nepenthes"]
  s.summary     = "Nepenthes Grids."

  s.files = Dir["{app,config,db,lib}/**/*"]
  s.metadata["rubygems_mfa_required"] = "true"
end
