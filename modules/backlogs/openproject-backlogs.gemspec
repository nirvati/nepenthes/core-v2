# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "openproject-backlogs"
  s.version     = "1.0.0"
  s.authors = "The Nepenthes developers"
  s.email = "nepenthes@nirvati.org"
  s.summary     = "Nepenthes Backlogs"
  s.description = "This module adds features enabling agile teams to work with Nepenthes in Scrum projects."
  s.files = Dir["{app,config,db,lib,doc}/**/*", "README.md"]

  s.add_dependency "acts_as_list", "~> 1.1.0"

  s.add_development_dependency "factory_girl_rails", "~> 4.0"
  s.metadata["rubygems_mfa_required"] = "true"
end
