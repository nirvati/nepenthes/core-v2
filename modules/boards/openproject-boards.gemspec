Gem::Specification.new do |s|
  s.name        = "openproject-boards"
  s.version     = "1.0.0"
  s.authors     = "The Nepenthes developers"
  s.email       = "nepenthes@nirvati.org"
  s.summary     = "Nepenthes Boards"
  s.description = "Provides board views"
  s.license     = "GPLv3"

  s.files = Dir["{app,config,db,lib}/**/*"]
  s.metadata["rubygems_mfa_required"] = "true"
end
