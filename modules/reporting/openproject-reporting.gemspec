Gem::Specification.new do |s|
  s.name        = "openproject-reporting"
  s.version     = "1.0.0"
  s.authors     = "The Nepenthes developers"
  s.email       = "nepenthes@nirvati.org"
  s.summary     = "Nepenthes Reporting"
  s.description = "This plugin allows creating custom cost reports with filtering and grouping created by the Nepenthes Costs plugin"

  s.files       = Dir["{app,config,db,lib,doc}/**/*", "README.md"]

  s.add_dependency "costs"
  s.metadata["rubygems_mfa_required"] = "true"
end
