Gem::Specification.new do |s|
  s.name        = "openproject-github_integration"
  s.version     = "1.0.0"
  s.authors     = "The Nepenthes developers"
  s.email       = "info@openproject.com"
  s.homepage    = "https://nepenthes.nirvati.org/system-admin-guide/integrations/github-integration/"
  s.summary     = "Nepenthes Github Integration"
  s.description = "Integrates Nepenthes and Github for a better workflow"
  s.license     = "GPLv3"

  s.files = Dir["{app,config,db,frontend,lib}/**/*"] + %w(README.md)

  s.add_dependency "openproject-webhooks"
  s.metadata["rubygems_mfa_required"] = "true"
end
