Gem::Specification.new do |s|
  s.name        = "openproject-documents"
  s.version     = "1.0.0"
  s.authors     = "The Nepenthes developers"
  s.email       = "nepenthes@nirvati.org"
  s.summary     = "Nepenthes Documents"
  s.description = "An Nepenthes plugin to allow creation of documents in projects"
  s.license     = "GPLv3"

  s.files = Dir["{app,config,db,lib,doc}/**/*", "README.md"]
  s.metadata["rubygems_mfa_required"] = "true"
end
