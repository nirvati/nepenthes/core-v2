module CaptchaLimitOverride
  def invalid_response?(resp)
    return super unless Nepenthes::Captcha::use_hcaptcha?

    resp.empty? || resp.length > ::Nepenthes::Captcha.hcaptcha_response_limit
  end
end

Recaptcha.singleton_class.prepend CaptchaLimitOverride
