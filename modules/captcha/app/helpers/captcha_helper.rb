module CaptchaHelper
  def captcha_available_options
    [
      [I18n.t("captcha.settings.type_disabled"), ::Nepenthes::Captcha::TYPE_DISABLED],
      [I18n.t("captcha.settings.type_v2"), ::Nepenthes::Captcha::TYPE_RECAPTCHA_V2],
      [I18n.t("captcha.settings.type_v3"), ::Nepenthes::Captcha::TYPE_RECAPTCHA_V3],
      [I18n.t("captcha.settings.type_hcaptcha"), ::Nepenthes::Captcha::TYPE_HCAPTCHA],
      [I18n.t("captcha.settings.type_turnstile"), ::Nepenthes::Captcha::TYPE_TURNSTILE]
    ]
  end

  def recaptcha_settings
    Setting.plugin_nepenthes_captcha
  end
end
