module ::Captcha
  class AdminController < ApplicationController
    include ::CaptchaHelper

    before_action :require_admin
    before_action :validate_settings, only: :update
    layout "admin"

    menu_item :plugin_captcha

    def show; end

    def update
      Setting.plugin_nepenthes_captcha = @settings
      flash[:notice] = I18n.t(:notice_successful_update)
      redirect_to action: :show
    end

    private

    def validate_settings
      new_params = permitted_params
      allowed_options = captcha_available_options.map(&:last)

      unless allowed_options.include? new_params[:captcha_type]
        flash[:error] = I18n.t(:error_code, code: "400")
        redirect_to action: :show
        return
      end

      @settings = new_params.to_h.symbolize_keys
    end

    def permitted_params
      params.permit(:captcha_type, :website_key, :secret_key, :response_limit)
    end

    def default_breadcrumb
      t("captcha.label_captcha")
    end

    def show_local_breadcrumb
      true
    end
  end
end
