require "recaptcha"
require "net/http"

module ::Captcha
  class RequestController < ApplicationController
    # Include global layout helper
    layout "no_menu"

    # User is not yet logged in, so skip login required check
    skip_before_action :check_if_login_required

    # Skip if recaptcha was disabled
    before_action :skip_if_disabled

    # Require authenticated user from the core to be present
    before_action :require_authenticated_user

    # Skip if user is admin
    before_action :skip_if_admin

    # Skip if user has confirmed already
    before_action :skip_if_user_verified

    # Ensure we set the correct configuration for rendering/verifying the captcha
    around_action :set_captcha_settings

    ##
    # Request verification form
    def perform
      if Nepenthes::Captcha::Configuration.use_hcaptcha?
        use_content_security_policy_named_append(:hcaptcha)
      elsif Nepenthes::Captcha::Configuration.use_turnstile?
        use_content_security_policy_named_append(:turnstile)
      elsif Nepenthes::Captcha::Configuration.use_recaptcha?
        use_content_security_policy_named_append(:recaptcha)
      end
    end

    def verify
      if valid_recaptcha?
        save_recaptcha_verification_success!
        complete_stage_redirect
      else
        fail_recaptcha I18n.t("captcha.error_captcha")
      end
    end

    private

    def set_captcha_settings(&)
      if Nepenthes::Captcha::Configuration.use_hcaptcha?
        Recaptcha.with_configuration(verify_url: Nepenthes::Captcha.hcaptcha_verify_url,
                                     api_server_url: Nepenthes::Captcha.hcaptcha_api_server_url,
                                     &)
      else
        yield
      end
    end

    ##
    # Insert that the account was verified
    def save_recaptcha_verification_success!
      # Remove all previous
      ::Captcha::Entry.where(user_id: @authenticated_user.id).delete_all
      ::Captcha::Entry.create!(user_id: @authenticated_user.id, version: recaptcha_version)
    end

    def recaptcha_version
      case recaptcha_settings["captcha_type"]
      when ::Nepenthes::Captcha::TYPE_DISABLED
        0
      when ::Nepenthes::Captcha::TYPE_RECAPTCHA_V2, ::Nepenthes::Captcha::TYPE_HCAPTCHA
        2
      when ::Nepenthes::Captcha::TYPE_RECAPTCHA_V3
        3
      when ::Nepenthes::Captcha::TYPE_TURNSTILE
        -1 # Just so we have something to put into the DB
      end
    end

    ##
    #
    def valid_recaptcha?
      if recaptcha_settings["captcha_type"] == Nepenthes::Captcha::TYPE_TURNSTILE
        verify_turnstile recaptcha_settings["secret_key"]
      else
        call_args = { secret_key: recaptcha_settings["secret_key"] }
        if recaptcha_version == 3
          call_args[:action] = "login"
        end

        verify_recaptcha call_args
      end
    end

    def verify_turnstile(secret_key)
      token = params["turnstile-response"]
      return false if token.blank?

      data = {
        "response" => token,
        "remoteip" => request.remote_ip,
        "secret" => secret_key,
      }

      data_encoded = URI.encode_www_form(data)

      response = Net::HTTP.post_form(
        URI("https://challenges.cloudflare.com/turnstile/v0/siteverify"),
        data
      )
      response = JSON.parse(response.body)
      response["success"]

    end

    ##
    # fail the recaptcha
    def fail_recaptcha(msg)
      flash[:error] = msg
      failure_stage_redirect
    end

    ##
    # Ensure the authentication stage from the core provided the authenticated user
    def require_authenticated_user
      @authenticated_user = User.find(session[:authenticated_user_id])
    rescue ActiveRecord::RecordNotFound
      Rails.logger.error "Failed to find authenticated_user for recaptcha verify."
      failure_stage_redirect
    end

    def recaptcha_settings
      Setting.plugin_nepenthes_captcha
    end

    def skip_if_disabled
      if recaptcha_settings["captcha_type"] == ::Nepenthes::Captcha::TYPE_DISABLED
        complete_stage_redirect
      end
    end

    def skip_if_admin
      if @authenticated_user&.admin?
        complete_stage_redirect
      end
    end

    def skip_if_user_verified
      if ::Captcha::Entry.exists?(user_id: @authenticated_user.id)
        Rails.logger.debug { "User #{@authenticated_user.id} already provided captcha. Skipping. " }
        complete_stage_redirect
      end
    end

    ##
    # Complete this authentication step and return to core
    # logging in the user
    def complete_stage_redirect
      redirect_to authentication_stage_complete_path :captcha
    end

    def failure_stage_redirect
      redirect_to authentication_stage_failure_path :captcha
    end
  end
end
