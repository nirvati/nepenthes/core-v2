Gem::Specification.new do |s|
  s.name        = "nepenthes-captcha"
  s.version     = "1.0.0"
  s.authors     = "The Nepenthes developers"
  s.email       = "nepenthes@nirvati.org"
  s.summary     = "Nepenthes Captcha"
  s.description = "This module provides Captcha checks during login"

  s.files = Dir["{app,config,db,lib}/**/*", "CHANGELOG.md", "README.rdoc"]

  s.add_dependency "recaptcha", "~> 5.7"
  s.metadata["rubygems_mfa_required"] = "true"
end
