module Nepenthes
  module Captcha
    TYPE_DISABLED ||= "disabled"
    TYPE_RECAPTCHA_V2 ||= "v2"
    TYPE_RECAPTCHA_V3 ||= "v3"
    TYPE_HCAPTCHA ||= "hcaptcha"
    TYPE_TURNSTILE ||= "turnstile"

    require "nepenthes/captcha/engine"
    require "nepenthes/captcha/configuration"

    extend Configuration
  end
end
