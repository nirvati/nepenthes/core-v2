require "open_project/plugins"
require "recaptcha"

module Nepenthes::Captcha
  class Engine < ::Rails::Engine
    engine_name :nepenthes_captcha

    include OpenProject::Plugins::ActsAsOpEngine

    register "nepenthes-captcha",
             author_url: "https://nepenthes.nirvati.org",
             settings: {
               default: {
                 captcha_type: ::Nepenthes::Captcha::TYPE_DISABLED,
                 response_limit: 5000
               }
             },
             bundled: true do
      menu :admin_menu,
           :plugin_captcha,
           { controller: "/captcha/admin", action: :show },
           parent: :authentication,
           caption: ->(*) { I18n.t("captcha.label_captcha") }
    end

    initializer "openproject.configuration" do
      ::Settings::Definition.add Nepenthes::Captcha::Configuration::CONFIG_KEY, default: false
    end

    config.after_initialize do
      SecureHeaders::Configuration.named_append(:recaptcha) do
        {
          frame_src: %w[https://www.recaptcha.net/recaptcha/ https://www.gstatic.com/recaptcha/]
        }
      end

      SecureHeaders::Configuration.named_append(:hcaptcha) do
        value = %w(https://*.hcaptcha.com)
        keys = %i(frame_src script_src style_src connect_src)

        keys.index_with value
      end

      SecureHeaders::Configuration.named_append(:turnstile) do
        value = %w(https://challenges.cloudflare.com)
        keys = %i(frame_src script_src style_src connect_src)

        keys.index_with value
      end

      OpenProject::Authentication::Stage.register(
        :captcha,
        nil,
        run_after_activation: true,
        active: -> { Nepenthes::Captcha.enabled? }
      ) do
        captcha_request_path
      end
    end
  end
end
