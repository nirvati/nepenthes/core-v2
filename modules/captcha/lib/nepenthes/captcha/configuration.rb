module Nepenthes
  module Captcha
    module Configuration
      CONFIG_KEY = "recaptcha_via_hcaptcha".freeze

      extend self

      def enabled?
        type.present? && type != ::Nepenthes::Captcha::TYPE_DISABLED
      end

      def use_hcaptcha?
        type == ::Nepenthes::Captcha::TYPE_HCAPTCHA
      end

      def use_turnstile?
        type == ::Nepenthes::Captcha::TYPE_TURNSTILE
      end

      def use_recaptcha?
        type == ::Nepenthes::Captcha::TYPE_RECAPTCHA_V2 || type == ::Nepenthes::Captcha::TYPE_RECAPTCHA_V3
      end

      def type
        ::Setting.plugin_nepenthes_captcha["captcha_type"]
      end

      def hcaptcha_response_limit
        (::Setting.plugin_nepenthes_captcha["response_limit"] || "5000").to_i
      end

      def hcaptcha_verify_url
        "https://hcaptcha.com/siteverify"
      end

      def hcaptcha_api_server_url
        "https://hcaptcha.com/1/api.js"
      end
    end
  end
end
