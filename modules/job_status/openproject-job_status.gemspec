Gem::Specification.new do |s|
  s.name        = "openproject-job_status"
  s.version     = "1.0.0"
  s.authors     = "The Nepenthes developers"
  s.email       = "nepenthes@nirvati.org"
  s.summary     = "Nepenthes Job status"
  s.description = "Listing and status of background jobs"
  s.license     = "GPLv3"
  s.metadata["rubygems_mfa_required"] = "true"
end
