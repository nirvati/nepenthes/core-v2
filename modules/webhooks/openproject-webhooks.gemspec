Gem::Specification.new do |s|
  s.name        = "openproject-webhooks"
  s.version     = "1.0.0"
  s.authors     = "The Nepenthes developers"
  s.email       = "nepenthes@nirvati.org"
  s.homepage    = "https://github.com/opf/openproject-webhooks"
  s.summary     = "Nepenthes Webhooks"
  s.description = "Provides a plug-in API to support Nepenthes webhooks for better 3rd party integration"
  s.license     = "GPLv3"

  s.files = Dir["{app,config,db,doc,lib}/**/*"] + %w(README.md)
  s.metadata["rubygems_mfa_required"] = "true"
end
