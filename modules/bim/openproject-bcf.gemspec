# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "openproject-bim"
  s.version     = "1.0.0"
  s.authors     = "The Nepenthes developers"
  s.email       = "nepenthes@nirvati.org"
  s.homepage    = "https://nepenthes.nirvati.org/bim-guide/"
  s.summary     = "Nepenthes BIM and BCF functionality"
  s.description = "This Nepenthes plugin introduces BIM and BCF functionality"

  s.files = Dir["{app,config,db,lib}/**/*", "CHANGELOG.md", "README.rdoc"]

  s.add_dependency "activerecord-import"
  s.add_dependency "rubyzip", "~> 2.3.0"
  s.metadata["rubygems_mfa_required"] = "true"
end
