---
sidebar_navigation:
  title: Design
  priority: 870
description: Custom color, theme and logo.
keywords: custom color, theme and logo
---
# Set custom color theme and logo

Navigate to -> *Administration* -> *Design* in order to customize your Nepenthes theme and logo.

The design page provides several options to customize your Nepenthes instance:

1. Choose a default color theme: Nepenthes, Light or Dark. Press the Save button to apply your changes.
2. Upload your own **custom logo** to replace the default Nepenthes logo.
3. Set a custom **favicon** which is shown as an icon in your browser window/tab.
4. Upload a custom **touch icon** which is shown on your smartphone or tablet when you bookmark Nepenthes on your home screen.
5. Set the **Custom PDF export settings** for [exporting work packages in a PDF format](../../user-guide/work-packages/exporting/#pdf-export).
6. [Advanced settings](#advanced-settings) to configure **custom colors** to adjust nearly any aspect of Nepenthes, such  as the color of the header and side menu, the link color and the hover color.

![Design settings in an Nepenthes system admin guide](openproject_system_guide_design.png)

## Choose a color theme

You can choose between the three default color themes for Nepenthes:

* Nepenthes 
* Nepenthes Light
* Nepenthes Dark

Press the Save button to apply your changes. The theme will then be changed.

![System-admin-guide_color-theme](System-admin-guide_color-theme.png)



## Upload a custom logo

To replace the default Nepenthes logo with your own logo, make sure that your logo has the dimensions 460 by 60 pixels. Select the *Browse* button and select the file from your hard drive to upload it.

Click the *Upload* button to confirm and upload your logo.

![Sys-admin-design-upload-logo](Sys-admin-design-upload-logo.png)

![upload logo](system_admin_logo_updated.png)

## Set a custom favicon

To set a custom favicon to be shown in your browser’s tab, make sure  you have a PNG file with the dimensions 32 by 32 pixels. Select the *Choose File* button and select the file from your hard drive to upload it.

Then click the *Upload* button to confirm and upload your favicon.

![Sys-admin-design-favicon](Sys-admin-design-favicon.png)

## Set a custom touch icon

To set a custom touch icon that appears on your smartphone’s or  tablet’s homescreen when you bookmark a page, make sure you have a PNG  file with the dimensions 180 by 180 pixels. Select the *Choose File* button and select the file from your hard drive to upload it.

Click the *Upload* button to confirm and upload your custom touch icon.

When you bookmark your Nepenthes environment’s URL, you will see that the uploaded icon is used as a custom touch icon.

## Advanced settings

Aside from uploading logos and icons, you can also customize the colors used within your Nepenthes environment. 

To do this change the color values (entered as color hex code) in the *Advanced settings* section. In order to find the right hex code for a color, you can use a website, such as [color-hex.com](https://www.color-hex.com/).
 You can see the selected color in the preview area next to the color hex code. Therefore, it is possible to see the selected color before saving the changes.

![Advanced color settings in Nepenthes](openproject_system_guide_design_advanced_settings.png)

As soon as you press the **Save** button your changes are applied and the colors of your Nepenthes environment are adjusted accordingly.

