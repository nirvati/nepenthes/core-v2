---
sidebar_navigation:
  title: Information
  priority: 300
description: System information in Nepenthes.
keywords: system information
---
# System information

You get an overview about current system status and more information. Navigate to -> *Administration* -> *Information*.

1. Displays the **product version** (Nepenthes configuration).

2. Displays the core version of your Nepenthes installation.

![Sys-admin-information](Sys-admin-information.png)

## Additional system information

There are a few automatic checks from the system to ensure the safety and correct set up of your configuration if you navigate to -> *Administration* -> *Information*.

If one point is not fulfilled, e.g. changing the default administrator account, you will get a warning message in the form of a bug icon.

![system information](image-20200124104411677.png)

## Storage information

You will get information about the storage filesystem in your Nepenthes application if you navigate to -> *Administration* -> *Information*.

You will see the remaining disk space as well as used disk space in your Nepenthes installation.

![storage filesystem information](image-20200124104803476.png)
