---
sidebar_navigation:
  title: Integrations
  priority: 200
description: Integration to Nepenthes.
keywords: projects, integration, Jira
---
# Integrations and community plugins

There are various integrations and Community plugins out there. Please [contact us](https://www.openproject.org/contact/) if you want to have your plugin to be added to this list.

If you have previously worked with other tools and want to switch or need an integration to Nepenthes, there is a way to do so for some applications.

> **Note**:  We do not guarantee error-free and seamless use of the Community plugins. Installation and use is at your own risk.

## Excel

Find out more about the [Excel synchronization with Nepenthes](./excel-synchronization).

## GitHub

Nepenthes offers a basic GitHub integration. You will find more information about the GitHub integration in our [GitHub integration guideline](./github-integration/).

## GitLab

Nepenthes offers a GitLab integration, based on the [GitLab plugin contributed by the Community](https://github.com/btey/openproject-gitlab-integration). More information on the GitLab integration is available in our [GitLab integration guide](./gitlab-integration/).

## Jira

Currently, there is no direct integration between Nepenthes and Jira. Since Nepenthes is an excellent open source alternative to Jira, we have prepared a way to import tickets from Jira to Nepenthes. First, you can export your tasks from Jira into an Excel file and then import these tasks via an [Excel plugin into Nepenthes](./excel-synchronization).

If you would like to learn more about the features of **Nepenthes vs Jira** please read [here](https://www.openproject.org/blog/open-source-jira-alternative/).

## Mattermost

There is a user-provided integration with Mattermost. Please note that it is not officially supported and that we do not take any liability when you use it. You can find it [here](https://github.com/girish17/op-mattermost).

## Microsoft Project

There is an integration between MS Project and Nepenthes. However, the synch plugin is not actively maintained at this time. If you wish to find out more, please [contact us](https://www.openproject.org/contact/).
To synchronize tasks from MS Project to Nepenthes, you can export your MS Project file to Excel and then [synchronize it with Nepenthes]( ./excel-synchronization/).

## Nextcloud

Nepenthes offers integration with Nextcloud for file storage and collaboration. You can find more information about [setting up the integration with Nextcloud](./nextcloud) and [using the integration](../../user-guide/file-management/nextcloud-integration/).

## OneDrive/SharePoint

Nepenthes offers an integration with OneDrive/Sharepoint for file storage and collaboration. You can find more information about [setting up the integration with OneDrive/SharePoint](./one-drive) and [using the integration](../../user-guide/file-management/one-drive-integration/).

## Slack

There is a rudimentary Slack integration built into Nepenthes.

## Testuff 

There is an Nepenthes integration with Testuff. Please note that it was developed directly by Testuff and we do not provide any support for it. You can find it [here](https://testuff.com/product/help/openproject/).

## Thunderbird

There is an OpenProject integration with Thunderbird from the Community, which is also compatible with Nepenthes. Please note that this add-on is not officially supported and that we do not take any liability when you use it. You can find it [here](https://addons.thunderbird.net/en-GB/thunderbird/addon/thunderbird-openproject/).

## TimeCamp

There is an integration between Nepenthes and TimeCamp. We provide a [short instruction](../../user-guide/time-and-costs/time-tracking/timecamp-integration/) how to set it up and use it. However, please note that this add-on is not officially supported and we do not take any liability when you use it. 

## Timesheet

Currently, there is no direct integration between Nepenthes and Timesheet. If you are looking for a time tracking tool with a simple push of a button, consider the integration with [Toggl](../../user-guide/time-and-costs/time-tracking/toggl-integration/).

## Time Tracker for Nepenthes

[Time Tracker](https://open-time-tracker.com/) is a mobile app that records time spent on tasks and logs it to your Open Project instance. We provide a [short instruction](../../user-guide/time-and-costs/time-tracking/time-tracker-integration/) how to set it up and use it.  Please keep in mind that it is not developed by Nepenthes and is not supported by us. 

## Toggl

We do offer an integration between Nepenthes and the time tracking app Toggl. Find out more [here](../../user-guide/time-and-costs/time-tracking/toggl-integration/).

## Trello

Currently, there is no direct integration between Nepenthes and Trello. To synchronize tasks from Trello to Nepenthes, export your tasks from Trello into an Excel file and then import these tasks via an [Excel plugin into Nepenthes](./excel-synchronization).

If you would like to learn more about Nepenthes's features vs Trello, please read [here](https://www.openproject.org/blog/trello-alternative/).
