---
sidebar_navigation:
  title: Integrations FAQ
  priority: 001
description: Frequently asked questions regarding integrations
keywords: integrations FAQ, apps, add-ons
---

# Frequently asked questions (FAQ) for integrations

## Is there an integration with Outlook?

No, we don't have an Outlook integration. Nevertheless, you can define incoming emails to Nepenthes to get converted into tasks. Also, you can configure the email notifications sent by Nepenthes to the project members. More information can be found [here](../../../installation-and-operations/configuration/outbound-emails).

## Is there an integration with OneNote?

No, there isn't.

## Is there a Slack integration?

Yes, there's a Slack plugin integrated into Nepenthes, although it only offers a very rudimentary integration.

## Can I migrate or synchronize boards from e.g. MS Project to Nepenthes?

You can synchronize the work packages in your board using the [Excel synchronization](../excel-synchronization). However, the boards themselves (the structure you built) can't be synced.
