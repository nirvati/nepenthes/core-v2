---
sidebar_navigation:
  title: File storages
  priority: 830
description: File storages in Nepenthes.
keywords: file storages, Nextcloud setup, Nextcloud integration, OneDrive setup, Sharepoint setup, OneDrive, Sharepoint
---

# File storages

You can connect your Nepenthes installation to Nextcloud or OneDrive/SharePoint. To do that navigate to **Administration** -> **Settings** -> **File storages** and select the respective option. 

You need to have administrator rights to be able to setup the integration.

![Files storages in Nepenthes administration](openproject_admin_guide_file_storages.png)

## Nextcloud integration setup guide

For detailed guide on the initial setup, please consult [Nextcloud integration setup guide](../integrations/nextcloud/).

Please also remember to activate the **File storages** module under [project settings in a respective project](../../user-guide/projects/project-settings/file-storages/).

For instructions on using the integration after the setup has been complete please refer to [Nextcloud integration user guide](../../user-guide/file-management/nextcloud-integration/).

## OneDrive/SharePoint integration setup guide

For detailed guide on the initial setup, please consult [OneDrive/SharePoint integration setup guide](../integrations/one-drive/).

Please also remember to activate the **File storages** module under [project settings in a respective project](../../user-guide/projects/project-settings/file-storages/).

For instructions on using the integration after the setup has been complete please refer to [SharePoint/OneDrive integration user guide](../../user-guide/file-management/one-drive-integration/).

## File storage troubleshooting

For troubleshooting guidance related to file storages, visit the [File storage troubleshooting](./file-storage-troubleshooting) page. Here you will find possible explanations and suggested solutions. If you encounter any  challenges not addressed here, do not hesitate to reach out to the [OpenProject community](https://community.openproject.org/projects/openproject/forums) or [support team](https://www.openproject.org/contact/) for further assistance.

