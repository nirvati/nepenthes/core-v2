---
sidebar_navigation:
  title: Custom fields for projects
  priority: 700
description: Custom fields for projects.
keywords: custom fields for projects, show custom fields
---
# Custom fields for projects

You can customize work package lists and show additional project information by adding custom attributes to project lists, e.g. adding accountable, project due date, progress, and more.

>**Important**: Starting with version 14.0, project custom fields are called "project attributes". Please refer to our [user guide on project attributes](../../../user-guide/project-overview) for more information. 
>
>If you are an administrator and wish to configure project attributes, please refer to our [admin guide on project attributes](../../projects/project-attributes).
