---
sidebar_navigation:
  title: Authentication
  priority: 900
description: Authentication in Nepenthes.
keywords: authentication
---
# Authentication

Configure **authentication** settings and authentication providers in Nepenthes.  To adapt these authentication settings, navigate to your user name and select -> *Administration* -> *Authentication*.

![Sys-admin-authentication](Sys-admin-authentication-1579787715984.png)

## Overview

| Topic                                                                        | Content                                                                               |
|------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------|
| [Authentication settings](authentication-settings)                           | Configure general authentication settings, such as registration, passwords, and more. |
| [OAuth applications](oauth-applications)                                     | How to configure OAuth applications in Nepenthes.                                   |
| [OpenID providers](openid-providers)                                         | How to configure OpenID providers in Nepenthes.                                     |
| [Two-factor authentication](two-factor-authentication)                       | Set up and manage two-factor authentication (2FA) in Nepenthes.                     |
| [reCAPTCHA](recaptcha)                                                       | How to activate reCAPTCHA in Nepenthes.                                             |
| [LDAP authentication](ldap-authentication)                                   | How to set up LDAP authentication in Nepenthes.                                     |
| [LDAP group synchronization](ldap-authentication/ldap-group-synchronization) | How to configure LDAP group synchronization in Nepenthes.         |

