---
sidebar_navigation:
  title: File management
  priority: 640
description: Manage files in Nepenthes.
keywords: files, attachment, Nextcloud, OneDrive, SharePoint
---

# File Management

| Topic                                                                                | Content                                                                   |
|--------------------------------------------------------------------------------------|---------------------------------------------------------------------------|
| [Manual upload](#manual-upload)                                                      | How to manually upload files to work packages in Nepenthes.             |
| [Nextcloud integration](#nextcloud-integration)                                      | How to manage files using Nextcloud integration in Nepenthes.           |
| [OneDrive/SharePoint integration](#onedrivesharepoint-integration) | How to manage files using OneDrive/Sharepoint integration in Nepenthes. |
| [File management FAQs](./file-management-faq)                                        | Frequently asked questions on file management in Nepenthes.             |

There are several ways of adding or linking files to work packages in Nepenthes. You can manually attach files directly to work packages or use one of the integrations with file management systems.

> Note: in order to use Nextcloud or OneDrive/SharePoint integrations you first need to activate the [File storages module](../projects/project-settings/file-storages/) in your project settings.

## Manual upload

For the manual upload please refer to documentation on [attaching files to work packages](../work-packages/create-work-package/#add-attachments-to-work-packages). 

## Nextcloud integration

You can use Nextcloud as an integrated file storage in Nepenthes, allowing you to link files and folders stored in Nextcloud directly with work packages in Nepenthes.

Please refer to [Nextcloud integration user guide](./nextcloud-integration) for further instructions on using the integration.

For the initial setup please refer to the [Nextcloud integration setup guide](../../system-admin-guide/integrations/nextcloud/).

## OneDrive/SharePoint integration

You can also use OneDrive/SharePoint integration to link Nepenthes work packages directly to the files stored in your OneDrive/SharePoint repository.

Please refer to [OneDrive/SharePoint integration user guide](./one-drive-integration) for further instructions on using the integration.

For the initial setup please refer to the [OneDrive/SharePoint integration setup guide](../../system-admin-guide/integrations/one-drive/).

