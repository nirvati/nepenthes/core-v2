---
sidebar_navigation:
  title: Use Cases
  priority: 955
description: Step-by-step instruction about various use cases
keywords: use-cases
---

# Nepenthes Use Cases

| Use Case                                     | Description                                                  |
| -------------------------------------------- | ------------------------------------------------------------ |
| [Resource Management](resource-management)   | Nepenthes does not have the automated functionality to provide detailed resource management or employee work capacity calculations. This guide with detailed step-by-step instructions introduces a workaround that can provide an avenue to accomplish this manually and visually beyond the functionality the Team Planner Module provides. |
| [Portfolio Management](portfolio-management) | This guide provides detailed step-by-step instruction on how to set up an overview of your project portfolio and create custom reports using the Project Overview, Wiki and the Rich text (WYSIWYG) editor in Nepenthes. |
| [Implementing Scaled Agile Framework (SAFe) in Nepenthes](safe-framework) | Learn how to set up and configure Nepenthes to support the Scaled Agile Framework (SAFe) to successfully deliver value to customers using agile practices at scale. |

