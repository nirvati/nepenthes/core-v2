---
sidebar_navigation:
  title: Operation
  priority: 200
---

# Operating Nepenthes

| Topic                              | Description                                                                   |
|------------------------------------|:------------------------------------------------------------------------------|
| [(Re)Configuring](./reconfiguring) | How to (re)configure your Nepenthes installation                            |
| [Backing up](./backing-up)         | How to backup your Nepenthes data                                           |
| [Restoring](./restoring)           | How to restore from a backup                                                  |
| [Upgrading](./upgrading)           | How to upgrade your Nepenthes installation                                  |
| [Monitoring & Logs](./monitoring)  | How to monitor your Nepenthes installation (logs, health checks)            |
| [Process control](./control)       | How to start/stop/restart, execute processes in your Nepenthes installation |
| [FAQ](./faq)                       | Frequently asked questions                                                    |

<!--
| [Troubleshooting](./troubleshooting) | How to troubleshoot your Nepenthes installation |
| [Scaling up/down](./scaling) | How to scale your Nepenthes installation |
-->
