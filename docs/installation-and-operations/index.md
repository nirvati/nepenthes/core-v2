---
sidebar_navigation:
  title: Installation & operations guide
  priority: 940
---

# Installation & operations guide

This page summarizes the options for getting Nepenthes. With this information you should be able to decide what option is best for you.

## Pre-built packages

| Main Topics                                 | Content                                                                                    |
|---------------------------------------------|:-------------------------------------------------------------------------------------------|
| [System requirements](system-requirements/) | Learn the minimum configuration required to run Nepenthes                                |
| [Installation](installation/)               | How to install Nepenthes and the methods available                                       |
| [Operations & Maintenance](operation/)      | Guides on how to configure, backup, **upgrade**, and monitor your Nepenthes installation |
| [Advanced configuration](configuration/)    | Guides on how to perform advanced configuration of your Nepenthes installation           |
| [Other](misc/)                              | Guides on infrequent operations such as MySQL to PostgreSQL migration                      |
| [BIM](../bim-guide/)                        | How to install Nepenthes BIM edition                                                     |

For production environments and when using a [supported distribution](system-requirements), we recommend using the [packaged installation](installation/packaged/). This will install Nepenthes as a system dependency using your distribution's package manager, and provide updates in the same fashion that all other system packages do.

## Manual installation

An OUTDATED and OLD [manual installation](installation/manual) option exists, but due to the large number of components involved and the rapid evolution of Nepenthes, we cannot ensure that the procedure is either up-to-date or that it will correctly work on your machine. This means that manual installation is NOT recommended NOR supported.

