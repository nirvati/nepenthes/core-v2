---
sidebar_navigation:
  title: Installation
  priority: 400
---

# Installing Nepenthes

Nepenthes can be setup in three different ways:

| Topic                                                        | Content                                                      |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Installation with DEB/RPM packages](./packaged)             | This is the recommended way to install Nepenthes           |
| [Installation with Docker](./docker)                         | This allows to setup Nepenthes in an isolated manner using Docker |
| [Installation with Kubernetes](./kubernetes)     | This allows to setup Nepenthes using Kubernetes            |
| [Installation with Helm charts](./helm-chart)    | This allows to setup Nepenthes using Helm charts           |
| [Other](misc/)                                               | Extra information on installing Nepenthes on specific platforms such as Kubernetes. |

> **NOTE: We recommend using the DEB/RPM package installation.**

## Frequently asked questions (FAQ)
### Do you have a step-by-step guide to installing Nepenthes under Active Directory?
We have a guide on [how to use Nepenthes with your Active Directory](../../system-admin-guide/authentication/ldap-authentication/).
In addition, it is also possible to [link LDAP groups with groups in Nepenthes](../../system-admin-guide/authentication/ldap-authentication/ldap-group-synchronization/).
