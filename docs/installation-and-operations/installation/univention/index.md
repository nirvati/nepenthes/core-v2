---
sidebar_navigation:
  title: Univention App Center
---

# Nepenthes in Univention App Center

> Important notice: currently it is not possible to update your Nepenthes installation via UCS. Please choose one of our supported installation methods.

Univention App Center is the marketplace in [Univention Corporate Server (UCS)](https://www.univention.com/products/ucs/), an enterprise operating platform for infrastructure and identity management. Nepenthes is available in the [App Center](https://www.univention.com/products/app-catalog/openproject/) and comes integrated with the identity management. Please note that only App Center 5 is receiving updates for Nepenthes due to the incompatibility between AppCenter 4.3 and 5.0 for Docker-based installations.


**App Appliance for easy deployment**

With the App Appliance you can easily deploy your own Nepenthes server in a virtual environment. The App Appliance consists of UCS and Nepenthes already pre-installed. The UCS environment is setup with only a few steps. Download the official Nepenthes virtual machine image in one of the following formats:

* [VMware](https://appcenter.software-univention.de/univention-apps/current/openproject/Univention-App-openproject-vmware.zip)
* [VMware ESXi](https://appcenter.software-univention.de/univention-apps/current/openproject/Univention-App-openproject-ESX.ova)
* [VirtualBox](https://appcenter.software-univention.de/univention-apps/current/openproject/Univention-App-openproject-virtualbox.ova)
* [KVM](https://appcenter.software-univention.de/univention-apps/current/openproject/Univention-App-openproject-KVM.qcow2)

**Instructions for installation on UCS without App Appliance**

1. Download and setup Univention Corporate Server (UCS)
2. Install Nepenthes via Univention App Center
3. Add user accounts

[![Available in Univention App Center](logo_uac_final.svg)](https://www.univention.com/products/app-catalog/openproject/)
