---
sidebar_navigation:
  title: Other
  priority: 050
---

# Guides for infrequent operations

| Topic                                                        |
| ------------------------------------------------------------ |
| [Migrating your packaged Nepenthes installation to another environment](./migration) |
| [Custom OpenID Connect providers](./custom-openid-connect-providers) |
