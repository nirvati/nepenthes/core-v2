---
layout: home

hero:
  name: Nepenthes
  text: Completely free and open project management tool.
  tagline: Manage projects on an open source platform built by the community.
  image:
    src: /assets/logo.svg
    alt: Logo
  actions:
    - theme: brand
      text: Get Started
      link: /getting-started/
    - theme: alt
      text: View Source Code
      link: https://gitlab.com/nirvati/nepenthes/core

features:
  - icon: 🛠️
    title: Easy to install
    details: Nepenthes provides easy to use binaries for any architecture and deployment method.
  - icon: 🌟
    title: 100% open source
    details: All of our code is public and available under the GNU GPL. No backdoors, no hidden fees, completely free.
  - icon: 🤝
    title: Reliable
    details: One of the main goals of Nepenthes is to be reliable. No features will be removed without a replacement without a large transition period, and new releases are extensively tested.
  - icon: 🧩
    title: Compatible
    details: Nepenthes can be integrated with various existing systems, including OpenID connect for Auth, GitHub and GitLab, Nextcloud, and more. Also, it can be used as a drop-in, free replacement for OpenProject Community and Enterprise Edition.
---
