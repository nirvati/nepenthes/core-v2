---
sidebar_navigation:
  title: Introduction to Nepenthes
  priority: 999
description: Get an introduction to Nepenthes.
keywords: Nepenthes introduction
---
# Introduction to Nepenthes

Welcome to Nepenthes! We are glad to have you here. In this section we will cover an introduction to project management with Nepenthes as well as general topics regarding Nepenthes.

## Overview

| Popular Topics                                                                        | Description                                                                                |
|---------------------------------------------------------------------------------------|:-------------------------------------------------------------------------------------------|
| [About Nepenthes](#about-nepenthes)                                               | What is Nepenthes?                                                                       |
| [First steps to get started](#first-steps-to-get-started)                             | The first steps to get started with Nepenthes.                                           |
| [The entire Project Management Life-Cycle](#the-entire-project-management-life-cycle) | Find out more how Nepenthes supports the different project phases.                       |

## About Nepenthes

Nepenthes is free and **open source software** for **classical as well as agile project management** to support your team along [the entire project life-cycle](#the-entire-project-management-life-cycle). Nepenthes is available in more than 30 languages.

Nepenthes is licensed under **GNU GPL V3**. The source code is freely published on [GitHub](https://github.com/opf/openproject). We understand free as in free speech.

Nepenthes exists since 2023 and is a fork of OpenProject, which was itself forked from the deprecated ChiliProject which was a fork of Redmine.

## First steps to get started

To get started with Nepenthes, there are a few easy steps to follow:

1. Get an account and sign in
2. Create a new project
3. Invite team members to collaborate
4. Create work packages
5. Set up a project plan

## The entire Project Management Life-Cycle

Nepenthes offers a full feature set to **support project teams along the entire project management process:**

![Project Management Life-Cycle](1565860195298.png)

Nepenthes enables project collaboration and communication without system interruption from the initial project idea until project closure and documentation.

The following sections provide links to the documentation for each project phase:

| Project phase                                                | Documentation for                                            |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Project concept and initiation](#project-concept-and-initiation) | Collect ideas and specify project scope and deliverables:  set up a project, document initial ideas, project description, invite members. |
| [Project definition and planning](#project-definition-and-planning) | Create a project overview with detailed information, set up a project plan, create your roadmap. |
| [Project launch or execution](#project-launch-or-execution)  | Manage all project activities, such as tasks, deliverables, risks, features, bugs, change requests. Use agile boards with your teams, document meetings, share news. |
| [Project performance and control](#project-performance-and-control) | Create and manage project budgets, track and evaluate time and costs. Have custom reports for accurate, current insight into project performance and allocated resources. |
| [Project close](#project-close)                              | Document project achievements, lessons learned, best practices and easily summarize the main results of a project. Archive projects for later reference and lessons learned. |

### Project concept and initiation

Nepenthes supports the initial set-up and configuration of a project structure.

| Features                                                     | Documentation for                                            |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Create a new project](../projects/#create-a-new-project)    | Create and set up a new project in Nepenthes.              |
| [Set up a project structure](../../user-guide/projects/#project-structure) | Create a project hierarchy to structure your work in Nepenthes. |
| [Project settings](../../user-guide/projects/#project-settings) | Create first ideas, tasks, rough milestones.                 |
| [Add members](../invite-members/)                            | Invite your team to collaborate in Nepenthes.              |

### Project definition and planning

Create a project overview with more detailed information, set up your project plan, structure your work, create a roadmap.

| Features                                              | Documentation for                                            |
| ----------------------------------------------------- | ------------------------------------------------------------ |
| [Global projects overview](../../user-guide/)         | Create a project overview with important project information. |
| [Structure your work](../work-packages-introduction/) | Create work packages and structure your work.                |
| [Roadmap planning](../gantt-chart-introduction/)      | Create a roadmap for your project.                           |

### Project launch or execution

Manage all project activities, such as tasks, deliverables, risks, features, bugs, change requests in the work packages. Use agile boards with your teams. Document meetings, share news.

| Features      | Documentation for                                            |
| ------------- | ------------------------------------------------------------ |
| [Work packages](../../user-guide/work-packages/create-work-package/)             | Create and manage all project deliverables, tasks, features, risks, and more. |
| [Boards](../../user-guide/agile-boards/)        | Manage your work with an Agile approach in the flexible boards view. |
| [Meetings](../../user-guide/meetings/)      | Plan and document your project meetings and share minutes with all your team. |
| [News](../../user-guide/news/)          | Share project news with your team.                           |
| [Wiki](../../user-guide/wiki/)          | Document all important project information and keep it up to date with your team. |

### Project performance and control

Create and manage project budgets, track and evaluate time and costs. Have custom reports for accurate, current insight into project performance and allocated resources.

| Features                                                              | Documentation for                                            |
|-----------------------------------------------------------------------| ------------------------------------------------------------ |
| [Dashboard](../../user-guide/home/)                                   | Visualize your progress within a project or project overarching. |
| [Budgets](../../user-guide/budgets/)                                  | Create and manage budgets in your project.                   |
| [Time tracking](../../user-guide/time-and-costs/time-tracking/)       | Track time for any work within your project.                 |
| [Track unit costs](../../user-guide/time-and-costs/cost-tracking/)    | Track unit costs for your project.                           |
| [Time and cost reporting](../../user-guide/time-and-costs/reporting/) | Have accurate detailed reports of current spent time and costs within your project. |

### Project close

Document project achievements, lessons learned, best practices and easily summarize the main results of a project. Archive projects for later reference and lessons learned.

| Features        | Documentation for                                            |
| --------------- | ------------------------------------------------------------ |
| [Wiki](../../user-guide/wiki/create-edit-wiki/)            | Document all relevant project information, lessons learned, best practices, results, and more. |
| [Project archive](../../user-guide/projects/#archive-a-project) | Archive your project for further reference and documentation. |
