---
sidebar_navigation:
  title: Getting started
  priority: 999
description: Nepenthes getting started guide.
keywords: getting started guide, tutorial
---

# Getting started guide

Welcome to the Nepenthes **Getting started guide**.

Here you will learn about the **first steps with Nepenthes**. If you need more detailed explanations of all features, please visit the respective section in our [user guide](../user-guide/).

## Overview

| Topic                                                    | Content                                                        |
|----------------------------------------------------------|:---------------------------------------------------------------|
| [Introduction to Nepenthes](openproject-introduction/)   | Get an introduction about project management with Nepenthes. |
| [Sign in and registration](sign-in-registration/)        | Find out how you can register and sign in to Nepenthes.      |
| [Create a project](projects/)                            | How to create and set up a new project.                        |
| [Invite team members](invite-members/)                   | How to invite new members.                                     |
| [Work packages](work-packages-introduction/)             | Learn how to create and edit work packages.                    |
| [Gantt chart](gantt-chart-introduction/)                 | Find out how to create a project plan.                         |
| [Boards](boards-introduction/)                           | How to work with agile boards.                                 |
| [My account](my-account/)                                | How to configure my account.                                   |
| [My page](my-page/)                                      | Find out more about a personal my page dashboard.              |

## 6 steps to get started

Watch a short 3-minute introduction video to get started with Nepenthes in 6 easy steps:

1. Create a project
2. Add team members
3. Create work packages
4. Create a project plan
5. Filter, group and generate reports
6. Create an agile board

<video src="https://openproject-docs.s3.eu-central-1.amazonaws.com/videos/Nepenthes-Getting-started.mp4" type="video/mp4" controls="" style="width:100%"></video>

## Nepenthes product demo video

Watch a **comprehensive Nepenthes product introduction** video to learn how to work with Nepenthes using traditional and agile project management.

<video src="https://openproject-docs.s3.eu-central-1.amazonaws.com/videos/Nepenthes-product-demo-webinar-2.mp4" type="video/mp4" controls="" style="width:100%"></video>

