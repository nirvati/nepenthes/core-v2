import { defineConfig } from "vitepress";
import { fileURLToPath } from "url";
import path from "path";
import { readFileSync } from "fs";

const __dirname = path.dirname(fileURLToPath(import.meta.url));
const sidebar = JSON.parse(
  readFileSync(path.resolve(__dirname, "..", "sidebar.json"), "utf-8")
);

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "Nepenthes",
  description: "A completely free and open source project management suite.",
  // TODO: Fix dead links
  ignoreDeadLinks: true,
  outDir: "../public",
  themeConfig: {
    editLink: {
      pattern: 'https://gitlab.com/nirvati/nepenthes/core/-/edit/dev/docs/:path'
    },
    search: {
      provider: "local",
    },
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      {
        text: "Getting started",
        link: "/getting-started/",
      },
      {
        text: "User guide",
        link: "/user-guide/",
      },
      {
        text: "BIM guide",
        link: "/bim-guide/",
      },
      {
        text: "Administration",
        items: [
          {
            text: "Installation & operations guide",
            link: "/installation-and-operations/",
          },
          {
            text: "System admin guide",
            link: "/system-admin-guide/",
          },
        ],
      },
      {
        text: "About",
        items: [
          {
            text: "Use Cases",
            link: "/use-cases/",
          },
          {
            text: "FAQ",
            link: "/faq/",
          },
          {
            text: "Glossary",
            link: "/glossary/",
          },
          {
            text: "Release notes",
            link: "/release-notes/",
          },
        ],
      },
      {
        text: "Developers",
        items: [
          {
            text: "Contributing",
            link: "/development/",
          },
          {
            text: "API documentation",
            link: "/api/",
          },
        ],
      },
      {
        text: "Blog",
        link: "/blog/",
      }
    ],

    sidebar,

    socialLinks: [
      {
        icon: {
          svg: '<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 256 256"><path fill="currentColor" d="M230.15 117.1L210.25 41a11.94 11.94 0 0 0-22.79-1.11L169.78 88H86.22L68.54 39.87A11.94 11.94 0 0 0 45.75 41l-19.9 76.1a57.19 57.19 0 0 0 22 61l73.27 51.76a11.91 11.91 0 0 0 13.74 0l73.27-51.76a57.19 57.19 0 0 0 22.02-61ZM58 57.5l15.13 41.26a8 8 0 0 0 7.51 5.24h94.72a8 8 0 0 0 7.51-5.24L198 57.5l13.07 50L128 166.21L44.9 107.5Zm-17.32 66.61L114.13 176l-20.72 14.65L57.09 165a41.06 41.06 0 0 1-16.41-40.89Zm87.32 91l-20.73-14.65L128 185.8l20.73 14.64ZM198.91 165l-36.32 25.66L141.87 176l73.45-51.9a41.06 41.06 0 0 1-16.41 40.9Z"/></svg>',
        },
        link: "https://gitlab.com/nirvati/nepenthes/core",
      },
    ],
  },
});
