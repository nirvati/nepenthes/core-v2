# Nepenthes 0.1.0

Today, we're excited to announce the release of Nepenthes 0.1.0!

Nepenthes is a drop-in replacement for OpenProject Enterprise Edition.

Nepenthes 0.1.0 is fully compatible with OpenProject 14.0.0, but includes various bug fixes and performance improvements.

Nepenthes is available for download at ....

In addition, we publish Docker images at `harbor.nirvati.org/nepenthes/core:0.1.0`.

## Differences from OpenProject

This current beta is very similar to OpenProject, but contains a few differences:

- **Support for Cloudflare Turnstile:** In addition to ReCAPTCHA and HCaptcha, Nepenthes also supports Cloudflare Turnstile for more convenient protection against spam.
- **Completely free:** All features of "OpenProject Enterprise Edition" are available for free in Nepenthes.
- **Different environment variables:** On OpenProject, if you wanted to configure settings via the environment, you had to use the `OPENPROJECT_` prefix. While this is still supported for Nepenthes, the `NEPENTHES` prefix should be used instead.
- **Performance improvements:** Nepenthes contains various performance improvements on the front-end and back-end that should make it slightly more responsive than openproject.
- **Bug fixes:** Nepenthes fixes a few minor bugs and typos in OpenProject.
