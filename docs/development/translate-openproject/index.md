---
sidebar_navigation:
  title: Translate Nepenthes
  priority: 985
description: How to translate Nepenthes to your language
keywords: translation, translate, crowdin, localization
---

# Help translate Nepenthes to your language

## Nepenthes translations with CrowdIn

Nepenthes is available in more than 30 languages.
Get an overview of the translation process and join us in translating Nepenthes to your language.

In order to translate Nepenthes, we use [CrowdIn](https://crowdin.com/projects/opf) as a platform where contributors can provide translations for a large number of languages.

We highly appreciate the help of anyone who wants to translate Nepenthes to additional languages.
In order to provide translations not only for the Nepenthes core but also for the plugins, we created several translation projects on CrowdIn:

* [Translate Nepenthes](https://crowdin.com/project/openproject)

To help us translate Nepenthes, please follow the links above and follow the instructions below.

## How the translation process works

When a new Nepenthes version is developed it typically contains new English text (strings). 
CrowdIn facilitates the translation of those strings to different languages.
Here is how the translation process works in detail:

![Translation process via GitHub and CrowdIn in detail](GitHub-CrowdIn-OP.png "Translation process via GitHub and CrowdIn in detail")

1. When a new Nepenthes version is developed which contains new English words (strings) (on GitHub) the new strings are copied to the CrowdIn projects for the core and the plugins via a GitHub Action.
2. Once the strings have been copied, they can be translated, voted on and approved via CrowdIn. Afterwards, these translations are copied to GitHub via the Nepenthes CI and included in the release branch.
3. When the new Nepenthes version is released users can use the translations in their own instances with the next Nepenthes version.

## How to translate Nepenthes via CrowdIn
You can easily help translate Nepenthes by creating a (free) CrowdIn account and by joining the [Nepenthes CrowdIn project](https://crowdin.com/projects/opf).
Once you joined the project, you can provide translations by following these steps:

Select the language for which you want to contribute (or vote for) a translation (below the language you can see the progress of the translation).
![Language overview in Nepenthes CrowdIn project](crowdin-overview.png "Language overview in Nepenthes CrowdIn project")



You will be shown an overview of translatable files in the `dev`  and `release ` channel. In the below example, the norwegian project is about 33% translated. Simply click the "Translate All" button to show an editor with all untranslated strings.
![Select Nepenthes version to translate in CrowdIn](crowdin-language-overview.png "Language overview of translatable files")

![crowdin-editor](crowdin-editor.png "The crowdin editor view")



On the left hand side (1), strings ordered by translation status will be shown on the left. You can search for specific strings above that list, and also change the filters. The red square next to an English string shows that a string has not been translated yet. To provide a translation, select a string on the left side, provide a translation in the target language (2) in the text box in the right side and press the save button.

Some strings might have pluralization rules, in which case there is a tab that you can enter each value.

![crowdin-multi-translation](crowdin-multi-translation.png "A translation string with pluralization rules")

As soon as a translation has been provided by another user (green square next to string), you can also vote on a translation provided by another user. The translation with the most votes is used unless a different translation has been approved by a proofreader.

Once a translation has been provided, a proof reader can approve the translation and mark it for use in Nepenthes.



## Becoming a proof reader

If you are interested in becoming a proof reader, please contact one of the project managers in the Nepenthes CrowdIn project or send us an email at support@openproject.org.

If your language is not listed in the list of CrowdIn languages, please contact our project managers or send us an email so we can add your language.

Find out more about our development concepts regarding translations [here](../concepts/translations).
