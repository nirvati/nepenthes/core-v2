---
sidebar_navigation:
  title: FAQ
  priority: 950
description: Frequently asked questions for Nepenthes (FAQ)
keywords: FAQ, introduction, tutorial, project management software, frequently asked questions, help
---
# Frequently asked questions (FAQ) for Nepenthes

Welcome to the central overview of frequently asked questions for Nepenthes.

| Topic                                                                           | Content                                                                                           |
|---------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| [Learn more about Nepenthes](#learn-more-about-openproject)                   | General questions about Nepenthes, security, setup and much more                                |
| [How to ... in Nepenthes](#how-to--in-openproject)                            | Questions about how to achieve certain outcomes in Nepenthes that do not fit elsewhere          |
| [FAQ regarding features](#faq-regarding-features)                               | Information about frequent feature requests                                                       |
| [FAQ regarding Nepenthes BIM edition](#faq-regarding-openproject-bim-edition) | Questions concerning the additional BCF module for Nepenthes and the BIM edition                |
| [Migration](#migration)                                                         | Questions regarding migrating to Nepenthes from e.g. Bitnami or from other Nepenthes versions |
| [Other](#other)                                                                 | Additional questions, e.g. about contribution, training, support                                  |
| [Topic-specific FAQ](#topic-specific-faq)                                       | Links to other FAQ sections                                                                       |


## Learn more about Nepenthes

### What are the system requirements?

The system requirements can be found [here](../installation-and-operations/system-requirements).

Nepenthes can be installed in two different ways: The packaged installation of Nepenthes is the recommended way to install and maintain Nepenthes using DEB or RPM packages. There's also a Docker based installation option.

### How can I learn more about Nepenthes?

Here are resources to get to know Nepenthes:

- The [overview of our features](https://www.openproject.org/collaboration-software-features)
- Our [English demo video](https://www.youtube.com/watch?v=un6zCm8_FT4) or [German demo video](https://www.youtube.com/watch?v=doVtVArSSvk) to get an overview of Nepenthes. There are additional videos explaining certain features to be found on our [YouTube channel](https://www.youtube.com/c/NepenthesCommunity/videos), too.
- The [Getting started guide](../getting-started) and the [User guide](../user-guide)
- Our [development roadmap](https://community.openproject.org/projects/openproject/roadmap) (to get to know future features)
- Our [training and consulting offers](https://www.openproject.org/training-and-consulting)

### Nepenthes is Open Source. Which kind of license does it come with? What am I allowed to do? What can I change?

Nepenthes comes with the GNU General Public License v3 (GPLv3). You can find out more about the copyright [here](https://github.com/opf/openproject/blob/dev/COPYRIGHT).
In accordance with the terms set by the GPLv3 license, users can make modifications, create copies and redistribute the work.
Terms and conditions regarding GPLv3 are available at [https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html) or in [our repository](https://github.com/opf/openproject/blob/dev/LICENSE).

### How many projects can I manage in Nepenthes at once?

The number of projects is always unlimited.

## How to ... in Nepenthes

Most of this kind of questions will be answered in the respective sections for each topic (see links below). However, there may be some FAQ that do not really fit elsewhere:

### I cannot log in, I do not know my password. What can I do?

As a first step please try to [reset your password](../getting-started/sign-in-registration/#reset-your-password). Please look in your spam folder, too, if you didn't receive an email.

If that doesn't help please contact your admin for login related topics. He/she can [set a new password for you](../system-admin-guide/users-permissions/users/#manage-user-settings).

### I cannot log in. Resetting my password seems to have no effect. What do I do?

Look in your spam folder for the email.

Ask your system admin to [set a new password for you](../system-admin-guide/users-permissions/users/#manage-user-settings).

If you are the system administrator, please have a look at [this FAQ](../installation-and-operations/operation/faq/#i-lost-access-to-my-admin-account-how-do-i-reset-my-password).

### How can I reverse changes?

This is not possible per se, there's no Ctrl+Z option or anything similar.

Please use these resources to find out about the latest changes and re-do them manually: The [work package activity](../getting-started/work-packages-introduction/#activity-of-work-packages), the [history of the wiki page](../user-guide/wiki/more-wiki-functions/#show-wiki-page-history) or the [Activities module](../user-guide/activity).

### How can I change the day my week starts with, etc.?

You can do this as a system administrator in the [System settings](../system-admin-guide/calendars-and-dates).

### How can I add a RACI matrix in Nepenthes?

You can add [project custom fields](../system-admin-guide/custom-fields/custom-fields-projects/) of the type "user" to your projects and track the respective persons there.

On a work package level you could use "Assignee" for "Responsible", "Accountable" for "Accountable" and [add custom fields](../system-admin-guide/custom-fields/) for "Consulted" and "Informed". For the latter one you could also just set the person as watcher instead.

### How can I create a PDF file with an individual and consolidated projects report?

To create and print/export reports you can...

- use the [global work packages list](../user-guide/projects/project-lists/#global-work-package-tables): Filter for e.g. phases and milestones (which would make sense to use in your projects in this case). Then use the [export feature](../user-guide/work-packages/exporting/). This will give you an overview over all projects' work packages (or all projects' milestones and phases, respectively).
- use the [Wiki module](../user-guide/wiki) to document your project reports. The Wiki pages is optimized for being printed using your browser's print feature. You could even insert a work packages list there. If you want to use the Wiki we suggest setting it up in a (global) parent project.

The projects overview is not optimized for export via PDF, yet. Nevertheless, you can try to use your browser's print feature.

### How can I receive the Nepenthes newsletter?

Please go to [openproject.org/newsletter/](https://www.openproject.org/newsletter/) and submit your data to receive our newsletter. Another option would be to agree to receive the newsletter when creating your account.

### Is it possible to use multiple languages in Nepenthes?

Yes, it is possible to use Nepenthes in multiple languages. We support English, German, French and a number of additional languages. Each user can select their own preferred language by signing into Nepenthes, clicking on the user avatar on the upper right side and selecting "My account" from the dropdown menu.
You can then select "Settings" from the side menu on the left side and [change the language](../getting-started/my-account/#change-your-language).

### Is there an Nepenthes app?

There is no native iOS or Android app for Nepenthes, but Nepenthes is responsive - so it displays well on smaller screens.

### Is it possible to connect MS Project and Nepenthes or to migrate from MS Project to Nepenthes?

Yes, please use the free [Excel synchronization](../system-admin-guide/integrations/excel-synchronization/) for this.

### Are there plan/actual comparisons in Nepenthes?

You can use the [Budgets module](../user-guide/budgets/#budgets) for a plan/actual comparison.

### Can I use Nepenthes offline?

No, it's not possible to use Nepenthes without network access to the server it is installed on.

### Can I import tasks from spreadsheets like Excel or LibreOffice?

Yes, that’s possible. Please have a look at our [Excel sync](../system-admin-guide/integrations/excel-synchronization/).

### Can I get a notification when a deadline approaches?

Yes, you can. Starting with Nepenthes 12.4 we implemented date alerts and email reminders about upcoming deadlines and overdue tasks. Please refer to [this guide](../user-guide/notifications/) for more details.

### Does Nepenthes offer resource management?

You can [set up budgets](../user-guide/budgets), [set the estimated time in  the **Work** field](../user-guide/work-packages/edit-work-package/) of a work package and use the [Assignee board](../user-guide/agile-boards/#choose-between-board-types) to find out how many work packages are assigned to a person at the moment.
Additional resource management features will be added within the next years. You can find the roadmap for future releases [here](https://community.openproject.org/projects/openproject/work_packages?query_id=1993).
More information regarding resource management in Nepenthes can be found in the [Use Cases](../use-cases/resource-management) section.

### Does Nepenthes offer portfolio management?

For portfolio management or custom reporting, you can use either the project list, or the global work package table. Both views can be used to create optimal reports via filtering, sorting and other configuration options.

For more information on portfolio management options in Nepenthes please refer to this [Use Case](../use-cases/portfolio-management).


### Is there an organizational chart in Nepenthes?

There's no such feature. However, you can use the wiki to add information regarding your departments and employees. Furthermore, you can upload existing org charts as image or e.g. PDF to the wiki or the documents module.

In many companies it makes sense to structure your project tree according to your departments (i.e. one parent project for each department with projects for each topic or client underneath).

### Is there an architecture diagram for Nepenthes?

No, currently we do not have such a diagram.

### Can I set up an entity-relationship diagram in Nepenthes?

No, currently we do not have an entity-relationship diagram for Nepenthes.

### Can I edit the Home page?

Only the welcome block/text can be edited, the rest cannot. However, you can [change the theme and logo](../system-admin-guide/design) of your Nepenthes instance.

## FAQ regarding Nepenthes BIM edition

### How can I find out more about Nepenthes BIM edition?

Please have a look at our [demo video](https://www.youtube.com/watch?v=ThA4_17yedg) and at our [website](https://www.openproject.org/bim-project-management/). You can start a free trial there, too.

### Which IFC format are you using for conversion in the BIM module?

IFC2x3 and IFC4. We accept those formats and convert them to a smaller format (XKT) that is optimized for browsing the models on the web.

### Can a BCF file created from other software e.g. BIMcollab, Solibri, etc. be opened in Nepenthes?

Yes, of course. That's why the module for this in Nepenthes is called "BCF". You can import and export BCF XML files. Our goal is to have specialized tools like Solibri do model checks, but the coordination of the results, the issues, is done in Nepenthes, because more people can get access to the BCF issues through Nepenthes because our licenses are much cheaper. In addition, BCF issues imported into Nepenthes behave just like other non-BCF work packages. For example, you can plan them in a Gantt chart on the timeline, or manage them agilely in boards. We support the current BCF XML format 2.1.
Furthermore, we are planning a direct integration into Solibri. Then you don't need to export and import XML files anymore, but Solibri will read and write directly into Nepenthes via an interface, the BCF-API. Before that, however, we will complete the integration in Autodesk Revit.
(Status: February 2021)

### Does clicking on a BCF-issue zoom you to the appropriate location in the model?

Yes, the so-called camera position is stored in the BCF-issues, so that exactly the same camera position is assumed when you click on the BCF-issue. These are called viewpoints. If you have several models, e.g. architecture and technical building equipment, these must be activated (made visible) before you click on the BCF-issue. In the same way, BCF-elements of the model can be hidden or selected via the viewpoint.

In our [introductory video](https://www.youtube.com/watch?v=ThA4_17yedg) to the Nepenthes BIM edition the basics are shown very well. In particular, the integration of BCF management into the rest of the project management of a construction project is the strength of Nepenthes.

### Can I add photos from my mobile/phone to BIM issues?

Yes. Take a photo with your camera and save it on your phone. Then open the correct work package in your browser or create a new one. Append the photo as an attachment to the work package.

### Can I use IFC while a Revit connection is not available?

Yes, of course. Within the BCF module you can upload multiple IFC models and create and manage BCF issues.


## Migration

### How can I migrate from Bitnami to Nepenthes?

To migrate from Bitnami, please use [this instruction](../installation-and-operations/installation-faq/#how-can-i-migrate-from-bitnami-to-the-official-nepenthes-installation-packages).

### How can I migrate from Jira/Confluence to Nepenthes?

At the moment there are these ways to migrate:

- our [API](../api/)
- our [Excel sync](../system-admin-guide/integrations/excel-synchronization)
- Using a [Markdown export app](https://marketplace.atlassian.com/apps/1221351/markdown-exporter-for-confluence) you can export pages from Confluence and paste them (via copy & paste) into Nepenthes in e.g. the wiki. This should preserve at least most of the layout. Attachments would then have to be added manually.

For more information please contact us.

### How can I migrate from an old version of Nepenthes to the latest version?

Nepenthes changed the database from MySQL (rarely also MariaDB) in older Versions and used PostgreSQL 10 afterwards. With the release of version 12 Nepenthes introduced the PostgreSQL 13 database. For further information on several database migrations, please have a look at [this section](../installation-and-operations/misc).

## Other

### How can I contribute to Nepenthes?

We welcome everybody willing to help make Nepenthes better. There are a lot of possibilities for helping, be it [improving the translations](../development/translate-openproject) via crowdin, answering questions in the [forums](https://community.openproject.org/projects/openproject/forums) or by fixing bugs and implementing features.

If you want to code, a good starting point would be to make yourself familiar with the [basic approaches for developing](../development/) in Nepenthes and opening a pull request on GitHub referencing an existing bug report or feature request. Find our GitHub page [here](https://github.com/opf/openproject).

If in doubt on how you should start, you can also just [contact us](https://www.openproject.org/contact/).

### How can I receive support?

<!-- TODO(NEPENTHES): Please feel free to use our [forums](https://community.openproject.org/projects/openproject/forums) for exchange with other users. -->

To learn more about Nepenthes and how its features work please have a look at [this FAQ](#how-can-i-learn-more-about-nepenthes).

## Topic-specific FAQ

Here are some selected links to other FAQ pages. Please use the menu to navigate to a topic's section to find more FAQs or use the search bar in the header.

- [FAQ for work packages](../user-guide/work-packages/work-packages-faq)
- [FAQ for Gantt chart](../user-guide/gantt-chart/gantt-chart-faq)
- [FAQ for system administration](../system-admin-guide/system-admin-guide-faq)
- [FAQ for installation, operation and upgrades](../installation-and-operations/installation-faq)
