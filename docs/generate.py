import os
import json
import re

def get_markdown_files(directory):
    markdown_files = []
    for root, dirs, files in os.walk(directory):
        if "node_modules" in root or ".vitepress" in root or ".git" in root:
            continue
        for file in files:
            if file.endswith(".md"):
                markdown_files.append(os.path.join(root, file))
    return markdown_files

def get_title(file_path):
    with open(file_path, 'r') as file:
        for line in file:
            match = re.match(r'#\s(.*)', line)
            if match:
                return match.group(1)
    return None

def create_sidebar_json(markdown_files):
    sidebar = {}
    for file_path in markdown_files:
        directory = os.path.dirname(file_path)
        # Split directory at / and get that as a "path" into the sidebar
        directory = directory.split('/')
        sidebar_ref = sidebar
        for path in directory:
            if "items" not in sidebar_ref:
                sidebar_ref["items"] = []
            last_sidebar_ref = sidebar_ref
            sidebar_ref = None
            for item in last_sidebar_ref["items"]:
                if ("link" in item and item["link"].endswith("/{}/".format(path))) or ("text" in item and item["text"] == path):
                    sidebar_ref = item
                    break
            if not sidebar_ref and not path.endswith("index.md"):
                sidebar_ref = {
                    "text": path
                }
                last_sidebar_ref["items"].append(sidebar_ref)

        heading = get_title(file_path)
        if file_path.endswith("index.md"):
          sidebar_ref["link"] = file_path.replace("index.md", "").removeprefix(".")
          sidebar_ref["text"] = heading
        elif heading:
            if not "items" in sidebar_ref:
                sidebar_ref["items"] = []
            sidebar_ref["items"].append({
                "link": file_path.replace("index.md", "").removeprefix("."),
                "text": heading
            })
        else:
            print(f"Could not find title in {file_path}")
            
    return sidebar

markdown_files = get_markdown_files('.')
sidebar = create_sidebar_json(markdown_files)

output = {}

for item in sidebar["items"][1]["items"]:
  if not "items" in item:
    print(f"Could not find items in {item['link']}")
  else:
    output[item["link"]] = item["items"]

with open('sidebar.json', 'w') as file:
    json.dump(output, file, indent=2)
